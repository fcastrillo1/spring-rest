FROM openjdk:8-jdk-alpine
ADD /target/spring-rest-app-1.0.0.jar app.jar
ENTRYPOINT ["java","-jar","/app.jar"]
